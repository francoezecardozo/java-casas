package modulo1;

public class Ejercicio3 {

	public static void main(String[] args) {
		System.out.println("Tecla de escape \t Significado\n");
		System.out.println("\\n \t Significado una nueva linea\n");
		System.out.println("\\t \t Significa un tab de espacio\n");
		System.out.println("\\\" \t Es para poner comillas dobles\n");
		System.out.println("\\\\ \t Se utiliza para poner la / dentro del texto\n");
		System.out.println("\\' \t Se utiliza para poner las comillas simple (��) dentro del texto\n");


	}

}
