package modulo4;
import java.util.Scanner;
public class Ejer9 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Para la competicion los valores son: \n Piedra = 0 \n Papel = 1 \n Tijera = 2\n");
		System.out.println("Ingrese el valor del primer participante");
		float PrimerCompetidor=scan.nextFloat();
		System.out.println("Ingrese el valor del segundo participante");
		float SegundoCompetidor=scan.nextFloat();
		
		if (PrimerCompetidor == SegundoCompetidor)
			System.out.println("Empataron");
		else
		{
			if (PrimerCompetidor == 0 && SegundoCompetidor == 2)
				System.out.println("Gana el primer competidor");
			else if (PrimerCompetidor == 0 && SegundoCompetidor == 1)
				System.out.println("Gana el segundo competidor");
			else if (PrimerCompetidor == 1 && SegundoCompetidor == 0)
				System.out.println("Gana el primer competidor");
			else if (PrimerCompetidor == 1 && SegundoCompetidor == 2)
				System.out.println("Gana el segundo competidor");
			else if (PrimerCompetidor == 2 && SegundoCompetidor == 0)
				System.out.println("Gana el segundo competidor");
			else if (PrimerCompetidor == 2 && SegundoCompetidor == 1)
				System.out.println("Gana el primer competidor");
		}
		scan=null;

	}

}
