package modulo4;
import java.util.Scanner;
public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una letrita");
		char Letra = scan.next().charAt(0);
		
		if (Letra == 'a' || Letra == 'e' || Letra == 'i' || Letra == 'o' || Letra == 'u' || Letra == 'A' || Letra == 'E' || Letra == 'I' || Letra == 'O' || Letra == 'U')
			System.out.println("La letra es una vocal");
		else
			System.out.println("La letra es una consonante");
		
		scan=null;

	}

}
