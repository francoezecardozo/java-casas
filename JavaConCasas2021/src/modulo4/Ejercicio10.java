package modulo4;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la primera variable");
		int PrimerValor =scan.nextInt();
		System.out.println("Ingrese la segunda variable");
		int SegundoValor =scan.nextInt();
		System.out.println("Ingrese la tercera variable");
		int TercerValor =scan.nextInt();
		
		if (PrimerValor > SegundoValor && SegundoValor > TercerValor)
			System.out.println("La primer variable " + PrimerValor + " es mayor");
		else if (SegundoValor > PrimerValor && SegundoValor > TercerValor)
			System.out.println("La segunda variable " + SegundoValor + " es mayor");
		else if (TercerValor > PrimerValor && TercerValor > SegundoValor)
			System.out.println("La tercera variable " + TercerValor + " es mayor");
		else
			System.out.println("No hay numero mayor");

		scan=null;

	}

}
